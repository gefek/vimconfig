" Maintainer:	Grzegorz Wilk
if !has('gui_running') && &t_Co < 256
  finish
endif

set background=dark
hi clear

if exists('syntax_on')
  syntax reset
endif

let g:colors_name = 'gefek'
" ColorColumn	used for the columns set with 'colorcolumn'
" 							*hl-Conceal*
" Conceal		placeholder characters substituted for concealed
" 		text (see 'conceallevel')
" 							*hl-Cursor*
" Cursor		the character under the cursor
" 							*hl-CursorIM*
" CursorIM	like Cursor, but used when in IME mode |CursorIM|
" 							*hl-CursorColumn*
" CursorColumn	the screen column that the cursor is in when 'cursorcolumn' is
" 		set
" 							*hl-CursorLine*
" CursorLine	the screen line that the cursor is in when 'cursorline' is
" 		set
" 							*hl-Directory*
" Directory	directory names (and other special names in listings)
" 							*hl-DiffAdd*
" DiffAdd		diff mode: Added line |diff.txt|
" 							*hl-DiffChange*
" DiffChange	diff mode: Changed line |diff.txt|
" 							*hl-DiffDelete*
" DiffDelete	diff mode: Deleted line |diff.txt|
" 							*hl-DiffText*
" DiffText	diff mode: Changed text within a changed line |diff.txt|
" 							*hl-EndOfBuffer*
" EndOfBuffer	filler lines (~) after the last line in the buffer.
" 		By default, this is highlighted like |hl-NonText|.
" 							*hl-ErrorMsg*
" ErrorMsg	error messages on the command line
" 							*hl-VertSplit*
" VertSplit	the column separating vertically split windows
" 							*hl-Folded*
" Folded		line used for closed folds
" 							*hl-FoldColumn*
" FoldColumn	'foldcolumn'
" 							*hl-SignColumn*
" SignColumn	column where |signs| are displayed
" 							*hl-IncSearch*
" IncSearch	'incsearch' highlighting; also used for the text replaced with
" 		":s///c"
" 							*hl-LineNr*
" LineNr		Line number for ":number" and ":#" commands, and when 'number'
" 		or 'relativenumber' option is set.
" 							*hl-CursorLineNr*
" CursorLineNr	Like LineNr when 'cursorline' or 'relativenumber' is set for
" 		the cursor line.
" 							*hl-MatchParen*
" MatchParen	The character under the cursor or just before it, if it
" 		is a paired bracket, and its match. |pi_paren.txt|
" 
" 							*hl-ModeMsg*
" ModeMsg		'showmode' message (e.g., "-- INSERT --")
" 							*hl-MoreMsg*
" MoreMsg		|more-prompt|
" 							*hl-NonText*
" NonText		'@' at the end of the window, characters from 'showbreak'
" 		and other characters that do not really exist in the text
" 		(e.g., ">" displayed when a double-wide character doesn't
" 		fit at the end of the line).
" 							*hl-Normal*
" Normal		normal text
" 							*hl-Pmenu*
" Pmenu		Popup menu: normal item.
" 							*hl-PmenuSel*
" PmenuSel	Popup menu: selected item.
" 							*hl-PmenuSbar*
" PmenuSbar	Popup menu: scrollbar.
" 							*hl-PmenuThumb*
" PmenuThumb	Popup menu: Thumb of the scrollbar.
" 							*hl-Question*
" Question	|hit-enter| prompt and yes/no questions
" 							*hl-QuickFixLine*
" QuickFixLine	Current |quickfix| item in the quickfix window.
" 							*hl-Search*
" Search		Last search pattern highlighting (see 'hlsearch').
" 		Also used for similar items that need to stand out.
" 							*hl-SpecialKey*
" SpecialKey	Meta and special keys listed with ":map", also for text used
" 		to show unprintable characters in the text, 'listchars'.
" 		Generally: text that is displayed differently from what it
" 		really is.
" 							*hl-SpellBad*
" SpellBad	Word that is not recognized by the spellchecker. |spell|
" 		This will be combined with the highlighting used otherwise.
" 							*hl-SpellCap*
" SpellCap	Word that should start with a capital. |spell|
" 		This will be combined with the highlighting used otherwise.
" 							*hl-SpellLocal*
" SpellLocal	Word that is recognized by the spellchecker as one that is
" 		used in another region. |spell|
" 		This will be combined with the highlighting used otherwise.
" 							*hl-SpellRare*
" SpellRare	Word that is recognized by the spellchecker as one that is
" 		hardly ever used. |spell|
" 		This will be combined with the highlighting used otherwise.
" 							*hl-StatusLine*
" StatusLine	status line of current window
" 							*hl-StatusLineNC*
" StatusLineNC	status lines of not-current windows
" 		Note: if this is equal to "StatusLine" Vim will use "^^^" in
" 		the status line of the current window.
" 							*hl-StatusLineTerm*
" StatusLineTerm	status line of current window, if it is a |terminal| window.
" 							*hl-StatusLineTermNC*
" StatusLineTermNC   status lines of not-current windows that is a |terminal|
" 		window.
" 							*hl-TabLine*
" TabLine		tab pages line, not active tab page label
" 							*hl-TabLineFill*
" TabLineFill	tab pages line, where there are no labels
" 							*hl-TabLineSel*
" TabLineSel	tab pages line, active tab page label
" 							*hl-Terminal*
" Terminal	|terminal| window (see |terminal-size-color|)
" 							*hl-Title*
" Title		titles for output from ":set all", ":autocmd" etc.
" 							*hl-Visual*
" Visual		Visual mode selection
" 							*hl-VisualNOS*
" VisualNOS	Visual mode selection when vim is "Not Owning the Selection".
" 		Only X11 Gui's |gui-x11| and |xterm-clipboard| supports this.
" 							*hl-WarningMsg*
" WarningMsg	warning messages
" 							*hl-WildMenu*
" WildMenu	current match in 'wildmenu' completion
" 
" Menu		Current font, background and foreground colors of the menus.
" 		Also used for the toolbar.
" 		Applicable highlight arguments: font, guibg, guifg.
" 
" 		NOTE: For Motif and Athena the font argument actually
" 		specifies a fontset at all times, no matter if 'guifontset' is
" 		empty, and as such it is tied to the current |:language| when
" 		set.
" 
" 							*hl-Scrollbar*
" Scrollbar	Current background and foreground of the main window's
" 		scrollbars.
" 		Applicable highlight arguments: guibg, guifg.
" 
" 							*hl-Tooltip*
" Tooltip		Current font, background and foreground of the tooltips.
" 		Applicable highlight arguments: font, guibg, guifg.
" 
" 		NOTE: For Motif and Athena the font argument actually
" 		specifies a fontset at all times, no matter if 'guifontset' is
" 		empty, and as such it is tied to the current |:language| when
" 		set.
"A      
highlight Normal         ctermfg=7 ctermbg=233 guibg=#121212 guifg=#c0c0c0
highlight Statement      ctermfg=220 cterm=bold gui=none guifg=#ffd700
highlight Constant       ctermfg=160 guifg=#df0000
highlight Number         ctermfg=166 guifg=#df5f00
highlight Type           ctermfg=34 guifg=#00af00
highlight Identifier     ctermfg=208 guifg=#ff8700
highlight NonText        ctermfg=2 guifg=#008000
highlight PreProc        ctermfg=130 guifg=#af5f00
highlight Todo           ctermfg=166 guifg=#df5f00 ctermbg=233 guibg=#121212

highlight SpecialKey ctermfg=245 guifg=#8a8a8a
highlight Search         ctermfg=11 ctermbg=53 cterm=bold guifg=#ffff00 guibg=#5f005f
highlight IncSearch      term=reverse ctermfg=15 ctermbg=53 gui=reverse guifg=white guibg=#5f005f
highlight Visual         cterm=reverse ctermbg=0 guibg=#000000
highlight CursorLine gui=NONE guibg=#262626 ctermbg=235 term=NONE cterm=NONE
highlight! link CursorColumn CursorLine
highlight Comment        ctermfg=33 guifg=#0087ff gui=NONE
highlight! link Directory      Comment
highlight StatusLine     cterm=bold gui=NONE ctermfg=7 ctermbg=33 guifg=#c0c0c0 guibg=#0087ff
highlight TabLine     cterm=bold gui=NONE ctermfg=7 ctermbg=33 guifg=#c0c0c0 guibg=#0087ff
"highlight StatusLineNC   ctermfg=black ctermbg=white cterm=NONE
"highlight User1          ctermfg=grey ctermbg=blue cterm=bold
"highlight VertSplit      ctermfg=gray ctermbg=blue cterm=bold
" 184 Yellow

highlight SpellBad       term=reverse ctermbg=52 gui=undercurl guisp=Red guibg=#5f0000
highlight Error          term=reverse ctermfg=15 ctermbg=52 guifg=White guibg=#5f0000
highlight ErrorMsg       term=standout ctermfg=15 ctermbg=52 guifg=White guibg=#5f0000
highlight WarningMsg     term=standout ctermfg=0 ctermbg=220 guifg=White guibg=#ffd700
"highlight vimCommand     ctermfg=Yellow ctermbg=black cterm=NONE guifg=#CF7520

"highlight Folded         term=standout cterm=bold ctermfg=6 ctermbg=0 guifg=Cyan guibg=black
highlight LineNr        term=underline ctermfg=2 guifg=green3
highlight Pmenu ctermfg=white ctermbg=darkblue gui=bold
highlight PmenuSel      ctermfg=white ctermbg=green gui=bold

highlight ColorColumn ctermbg=cyan guibg=DarkCyan

highlight SignColumn term=standout ctermfg=4 ctermbg=Black guifg=DarkBlue guibg=Black
highlight GitGutterAdd ctermfg=Green ctermbg=Black cterm=bold guifg=Green guibg=Black
highlight GitGutterChange ctermfg=Yellow ctermbg=Black cterm=bold guifg=#bbbb00 guibg=Black
highlight GitGutterDelete ctermfg=Red ctermbg=Black cterm=bold guifg=Red guibg=Black
highlight GitGutterChangeDelete ctermfg=DarkGreen ctermbg=Black cterm=bold guifg=Red guibg=Black

highlight DiffAdd        term=bold ctermbg=17 guibg=#00005f
highlight DiffChange     term=bold ctermbg=53 guibg=#5f005f
highlight DiffDelete     term=bold ctermfg=12 ctermbg=52 gui=bold guifg=Blue guibg=#5f0000
highlight DiffText       term=reverse cterm=bold ctermbg=88 gui=bold guibg=#870000

highlight! link StorageClass Type
highlight! link Structure Type
highlight! link Typedef Type
highlight! link pythonStatement Statement
highlight! link pythonFunction Function
highlight! link pythonConditional Conditional
highlight! link pythonRepeat   Repeat
highlight! link pythonOperator Operator
highlight! link pythonException Exception
highlight! link pythonInclude  Statement
highlight! link pythonAsync    Statement
highlight! link pythonDecoratorName Function
highlight! link pythonDecorator Define
highlight! link pythonTodo     Todo
highlight! link pythonComment  Comment
highlight! link pythonQuotes   Type
highlight! link pythonEscape   Special
highlight! link pythonString   String
highlight! link pythonTripleQuotes pythonQuotes
highlight clear pythonSpaceError
highlight! link pythonDoctest  Special
highlight! link pythonRawString String
highlight! link pythonNumber   Number
highlight! link pythonBuiltin  Function
highlight! link pythonAttribute Constant
highlight! link pythonExceptions Structure
highlight! link pythonDoctestValue Define
highlight clear pythonSync

highlight! link javascriptNumber   Number
highlight! link javaScriptParens Type

