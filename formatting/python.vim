setlocal colorcolumn=120

"highlight OverLength term=inverse cterm=underline guibg=#444444
"match OverLength /\%120v.\+/

":map <F6> :!pylint --disable-msg=C0111 "%"<CR>
":map <F10> :!pep8 "%" --max-line-length 119 --statistics<CR>

"source /home/users/gwilk/opt/share/vim/vim72/plugin/pythoncomplete.vim
setlocal omnifunc=pythoncomplete#Complete

